#ifndef BMP_H
#define BMP_H

#include "image.h"

#include <stdio.h>

enum read_status{
    READ_OK = 0,
    READ_ERROR
};

enum read_status from_bmp(FILE* f, struct image* image);

enum write_status{
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp(FILE* f, struct image* image);

#endif
