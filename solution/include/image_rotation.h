#ifndef IMAGE_ROTATION_H
#define IMAGE_ROTATION_H

#include "image.h"

#include <stdbool.h>

struct image rotation(struct image const image);

#endif
