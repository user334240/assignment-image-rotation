#ifndef IMAGE_H
#define IMAGE_H

#include <inttypes.h>

struct pixel{
    uint8_t b, g, r;
};

struct image{
    int64_t width, height;
    struct pixel* data;
};

struct image image_create(int64_t width, int64_t height);
void image_destroy(struct image* image);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
