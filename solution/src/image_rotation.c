#include "../include/image_rotation.h"

struct image rotation(struct image const image){
    int64_t new_width = image.height;
    int64_t new_height = image.width;
    struct image result = image_create(new_width, new_height);
    for (int64_t i = 0; i < new_width; i++){
        for (int j = 0; j < new_height; j++){
             uint64_t new_index = i + j * new_width;
             uint64_t old_index = j + image.height * image.width - image.width * (i + 1);
             result.data[new_index] = image.data[old_index];
        }
    }
    return result;
}
