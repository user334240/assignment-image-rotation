#include "image.h"
#include "bmp.h"
#include "image_rotation.h"

#include <errno.h>
#include <stdio.h>

int main( int argc, char** argv ) {
    errno = 0;
    (void) argc; (void) argv;
    if (argc != 3){
        fprintf(stderr, "wrong amount of arguments");
    }

    FILE* f = fopen(argv[1], "rb");
    if (!f){
        fprintf(stderr, "file-open error");
    }

    struct image image = {0};
    if (from_bmp(f, &image) == READ_ERROR){
        fprintf(stderr, "read error");
    }
    fclose(f);

    struct image result = rotation(image);
    image_destroy(&image);
    FILE* res_file = fopen(argv[2], "wb");

    if (to_bmp(res_file, &result) == WRITE_ERROR){
        fprintf(stderr, "write error");
    }
    fclose(res_file);

    image_destroy(&result);
    return 0;
}
