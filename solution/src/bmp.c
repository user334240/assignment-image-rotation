#include "../include/bmp.h"

#include <stdbool.h>

struct  __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

static int64_t padding(int64_t width){
    int64_t value = (width % 4 == 0 ? 0 : 4 - ((width * 3) % 4));
    return value;
}

static uint32_t file_sz(struct image* image){
    return image -> height * image -> width * sizeof(struct pixel) +
           image -> height * padding(image -> width) +
           sizeof(struct bmp_header);
}

static struct bmp_header create_header(struct image* image){
    struct bmp_header h = {0};
    h.bfType = 19778;
    h.bfileSize = file_sz(image);
    h.bOffBits = 54;
    h.biSize = 40;
    h.biWidth = image -> width;
    h.biHeight = image -> height;
    h.biPlanes = 1;
    h.biBitCount = 24;
    h.biSizeImage = file_sz(image) - 54;
    return h;
}

static bool convert(FILE* f, const struct bmp_header* h, struct image* image){
    *image = image_create(h->biWidth, h->biHeight);
    for (int64_t i = 0; i < image -> height; i++){
        for (int64_t j = 0; j < image -> width; j++){
            struct pixel pixel = {0};
            if (!fread(&pixel, sizeof(pixel), 1, f)){
                fprintf(stderr, "read error");
                image_destroy(image); //destroying image
                return false;
            }
            else{
                image -> data [j + i * image -> width] = pixel;
            }
        }
        if (padding(image -> width) != 0){
           if(!fseek(f, padding(image -> width), SEEK_CUR)){
               fprintf(stderr, "fseek error");
           }
        }
    }
    return true;
}

enum read_status from_bmp(FILE* f, struct image* image){
    if (f == NULL){
        return READ_ERROR;
    }
    else{
        struct bmp_header h ={0};
        enum read_status flag = ((!fread(&h, sizeof(h), 1, f) ||
                                  h.bfType != 19778 || !convert(f, &h, image)) ? READ_ERROR : READ_OK);
        return flag;
    }
}

static bool padding_addition(FILE* f, int64_t width){
    int8_t empty_byte = 0;
    for (int64_t i = 0; i < padding(width); i++){
        if (!fwrite(&empty_byte, sizeof(empty_byte), 1, f)){
            fprintf(stderr, "write error");
            return false;
        }
    }
    return true;
}

enum write_status to_bmp(FILE* f, struct image* image){
    struct bmp_header h = create_header(image);
    if (!fwrite(&h, sizeof(h), 1, f)){
        return WRITE_ERROR;
    }
    else{
        for (int64_t i = 0; i < image -> height; i++){
            for (int64_t j = 0; j < image -> width; j++){
                struct pixel pix = image -> data[j + i * image -> width];
                if (!fwrite(&pix, sizeof(pix), 1, f)){
                    return WRITE_ERROR;
                }
            }
            if (!padding_addition(f, image -> width)){
                return WRITE_ERROR;
            }
        }
    }
    return WRITE_OK;
}

