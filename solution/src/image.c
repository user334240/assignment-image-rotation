#include "../include/image.h"

#include <mm_malloc.h>

struct image image_create(int64_t width, int64_t height){
    struct image new_image = {0};
    new_image.width = width;
    new_image.height = height;
    new_image.data = malloc(sizeof(struct pixel) * width * height);
    return new_image;
}

void image_destroy(struct image* image){
    free(image -> data);
}
